#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define x 200
#define y 150

void imprimir(int matriz[x][y], int filas, int columnas){
    int i,j;
    //Recorremos la matriz
    for(i=0;i<filas;i++){
    	 printf("\n");
        for(j=0;j<columnas;j++){
            //Si es 0 la celula esta muerta, por lo que se imprime un punto
            if(matriz[i][j]==0){
                printf(" . "  );
            }
            
            if(matriz[i][j]==1){
                printf(" * ");
            }
            //printf("%d ", matriz[i][j]);
        }
    }
     printf("\n");
}

void crearMatriz(int matriz[x][y], int filas,int columnas)
{
	int i,j,numero, N=1;

	printf("ESTA ES LA PRIMERA GENERACION GENERADA ALEATORIAMENTE\n");
	for (i = 0; i <filas;i++)
	{
		for (j = 0; j<columnas; j++)
		{
			numero = rand() % 2; 
			numero = rand() % (N+1);
			matriz[i][j]=numero;
			//matriz[i][j]=1;
		}
	}
}
void lee(int matriz[x][y], int filas, int columnas){
	int i,k;
 	for ( i = 0; i < filas; i++)
		{
			for (k = 0; k < columnas; k++)
			{
				matriz[i][k]=0;
			}
	}
	FILE* archivo;
	char linea[100];
	archivo = fopen("vivoss.txt", "r");
	if(archivo == NULL){
		printf("Error: No se puede leer el archivo " );	
	}else{
		unsigned int strx = 0, stry = 0;
		while(fgets(linea, 100, archivo) != NULL){
			sscanf(linea, "%u,%u", &strx, &stry);
			if(strx >= 0 && strx < columnas && stry >= 0 && stry < filas){
				matriz[stry][strx] = 1;
			}else{
				printf("Error: Ha introducido una celda en el archivo pero el numero de filas o columnas es excedido por la coordenada de dicha celda (%d, %d)", strx, stry);	
				exit(0);
			}
		}
	}
}

void evaluar(int matriz[x][y] ,int filas,int columnas){
	int i, j, vivos=0, izq,der,arriba,abajo;
	int copia[x][y];

	for (i = 0; i < filas; i++)
	{	
		for ( j = 0; j < columnas; j++)
		{	vivos=0;
		
			if (i>0 & j>0 )
			{
				if(matriz[i-1][j-1]==1)vivos++;//1
				
			}
			if (i>0)
			{
				if(matriz[i-1][j]==1)vivos++;//2
			}
			
			if (i>0 & j<columnas-1 )
			{
				if(matriz[i-1][j+1]==1)vivos++;//3
			}
			if (j>0)
			{
				if(matriz[i][j-1]==1)vivos++;//4
			}
			if (j<columnas-1 )
			{
				if(matriz[i][j+1]==1)vivos++;//6
			}
			if (i<filas-1 & j>0 )
			{
				if(matriz[i+1][j-1]==1)vivos++;//7
				
			}
			if(i<filas-1){
				if(matriz[i+1][j]==1) vivos++;//8
			}

			if (i<filas-1 & j<columnas-1)
			{
				if(matriz[i+1][j+1]==1)vivos++;//9
			}
				//comparar las celulas vivas por las que esta rodeada
				if (vivos<2 || vivos>3)
				{
					copia[i][j]=0;
					//printf("se evaluo el punto %d  %d  y tiene %d vivos la celular murio su estado es %d \n",i,j, vivos, copia[i][j]);
				}
				if (vivos==3)
				{
					copia[i][j]=1;
					//printf("se evaluo el punto %d  %d  y tiene %d vivos la celula vive su estado es  %d  \n",i,j, vivos, copia[i][j]);
				}
				if(vivos==2){
					copia[i][j]=matriz[i][j];
					//printf("se evaluo el punto %d  %d  y tiene %d vivos  su estado es  %d  \n",i,j, vivos, copia[i][j]);
				}
		}
	}
	imprimir(copia,filas,columnas);
	for (i = 0; i < filas; i++)
	{
		for (j = 0; j < columnas; j++)
		{
			matriz[i][j]=copia[i][j];
		}
	}
}

main()
{	
	int filas,i, columnas, generaciones;
	char opcion;


	printf("dame el numero de filas \n");
	scanf(" %d", &filas);
	fflush(stdin);
	printf("dame el numero de columnas \n");
	scanf(" %d", &columnas);
	fflush(stdin);
	printf("dame el numero de generaciones que deseas \n");
	scanf(" %d", &generaciones);
	fflush(stdin);
	int matriz[x][y];
	printf("opciones  \n a.- generar matriz aleatoria \n\n b.-generar la matriz desde el archivo\n");
	scanf(" %c", &opcion);
	fflush(stdin);

	switch ( opcion )
   {
      case 'a' : 	crearMatriz(matriz,filas,columnas);
               break;
      case 'b' : lee(matriz,filas,columnas);
               break;
   }
	
   	printf("\nprimer matriz \n");
	imprimir(matriz,filas,columnas);
	for ( i = 0; i < generaciones; i++)
	{
		printf("generacion numero %d \n",i+1);
		evaluar(matriz,filas,columnas);
	}
	
	return 0;
}
